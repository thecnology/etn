<?php

namespace App\Presenters;

use Nette;
use App\Model\GitHubUsersRepo;
use Nette\Application\UI\Form;

class GithubPresenter extends BasePresenter {

    /** @var GitHubUsersRepo */
    private $model;

    /** @var \Nette\Http\Request @inject */
    public $httpRequest;

    public function __construct(GitHubUsersRepo $GitHubUsersRepo) {
        $this->model = $GitHubUsersRepo;
    }

    public function renderDefault() {
        
    }

    /**
     * create search form
     * @return Form
     */
    protected function createComponentGithubForm() {
        $form = new Form;

        $form->addText('username', 'Username:')
                ->setRequired();
        $form->addSubmit('send', 'Find user repositories');
        $form->onSuccess[] = [$this, 'githubtFormSucceeded'];
        return $form;
    }

    /**
     * onSuccess exec form and execute to add into model
     * @param type $form
     * @param type $values
     */
    public function githubtFormSucceeded($form, $values) {
        $this->flashMessage('Search succesfull', 'success');
        $username = $values->username;
        $this->template->username = $username;
        $client = new \Github\Client();
        $repositories = $client->api('user')->repositories($username);
        $this->model->addSearchUser($username, $this->httpRequest->getRemoteAddress());

        foreach ($repositories as $repo) {
            if ($repo['fork'] == "") {
                $this->model->addRepository($repo['name'], $repo['description'], $repo['html_url'], $repo['created_at']);
            }
        }
        $this->template->repositories = $this->model->getRepositoriesByLastSearch();
       
    }

}
