<?php

namespace App\Presenters;

use Nette;
use App\Model\GitHubUsersRepo;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use App\Forms;

class ChangePresenter extends BasePresenter {

    /** @var GitHubUsersRepo */
    private $model;

    /** @var Forms\SignInFormFactory @inject */
    public $signInFactory;

    /** @var Forms\SignUpFormFactory @inject */
    public $signUpFactory;

    public function __construct(GitHubUsersRepo $GitHubUsersRepo) {
        $this->model = $GitHubUsersRepo;
    }

    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm() {
        return $this->signInFactory->create(function () {
                    $this->redirect('Change:Default');
                });
    }

    /**
     * Sign-up form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignUpForm() {
        return $this->signUpFactory->create(function () {
                    $this->redirect('Change:Default');
                });
    }

    /**
     * Logout method
     */
    public function actionOut() {
        $this->getUser()->logout();
    }

    /**
     * create component with delete form
     * @return Form
     */
    protected function createComponentDeleteForm() {
        $form = new Form;

        $form->addText('time', 'Time - h:')
                ->setRequired();
        $form->addSubmit('send', 'Remove searches after search in hours');
        $form->onSuccess[] = [$this, 'deleteFormSucceeded'];
        return $form;
    }

    /**
     * on successfully submited form, model remove searches after submited value
     * @param type $form
     * @param type $values
     */
    public function deleteFormSucceeded($form, $values) {
        $time = $values->time;
        if (is_numeric($time) && $time > 0) {
            $this->flashMessage('Removing search that is older than ' . $time . " hour from now", 'success');
            $time = $values->time;
            $this->template->time = $time;
            $this->template->removedRows = $this->model->removeSearchesOldThanHours($time);
        } else {
            $this->flashMessage('U must set value>0', 'false');
            $this->template->time = "0";
        }
    }

    public function renderDefault() {
        
    }

}
