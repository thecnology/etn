<?php

namespace App\Presenters;

use Nette;
use App\Model\GitHubUsersRepo;
use Nette\Application\UI\Form;

class ShowPresenter extends BasePresenter {

    /** @var GitHubUsersRepo */
    private $model;

    public function __construct(GitHubUsersRepo $GitHubUsersRepo) {
        $this->model = $GitHubUsersRepo;
    }

    public function renderDefault() {

        $paginator = new Nette\Utils\Paginator;
        $count=$this->model->getRepositoriesCount();
        $paginator->setItemCount($count);
        $paginator->setItemsPerPage(30);
        $get = $this->request->getParameters();
        if (!isset($get['page'])) {
            $paginator->setPage(1);
        } else {
            $paginator->setPage($get['page']);
        }
        $repos = $this->model->getRepositoriesFromDB($paginator);
        $this->template->searches = $repos;
        $this->template->total = $count;
        $this->template->searchedRows =$this->model->getSearchCount();
        $this->template->paginator = $paginator;
    }

}
