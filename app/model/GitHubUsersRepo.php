<?php
/**
 * GitHubUsersRepo model
 *
 * 
 * @author Vitezslav Kis >> thecnology[at]gmail.com
 */

namespace App\Model;

use Nette;

class GitHubUsersRepo {

    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */
    private $database;

    /**
     * searched user
     */
    private $user;

    /**
     * date of search 
     */
    private $searchDate;

    /**
     * list of seacrhed repositories
     */
    private $repo = [];

    /**
     * last inserted id of pk [ github_search.id ]
     */
    private $searchId;

    /**
     * query for getting repositories over searches ordered by searched date desc
     */
    private $searchQ = "select A.user,A.searchdate,A.ip,B.id,B.search_id,B.name as 'repo',B.description,B.url,B.create_date from github_search A left outer join github_repo as B on A.id=B.search_id order by A.searchdate desc";

    /**
     * ip adress used on search
     */
    private $ip;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    /**
     * add search user on github and save it into database with timestamp
     * @param String $user
     */
    public function addSearchUser($user, $ip) {
        $this->user = $user;
        $this->searchDate = date("Y-m-d H:i:s");
        $this->ip = $ip;
        $this->saveSearch();
    }

    /**
     *  add repository params and save it into database
     * @param type $repo - name of repo
     * @param type $description - description of repo
     * @param type $url - url of repo
     * @param type $created - date of creation 
     */
    public function addRepository($repo, $description, $url, $created) {
        $datetime = new Nette\Utils\DateTime($created);
        $datetime->format("Y-m-d H:i:s");
        $repoArr = ["repo" => $repo, "description" => $description, "url" => $url, "created" => $datetime];
        $this->repo[] = $repoArr;
        $this->saveRepo($repoArr);
    }

    /**
     * Return array with repositories loaded from API but not sorted DESC
     * @return array
     */
    public function getRepositories() {
        return $this->repo;
    }
    /**
     * it will return array of sorted repositories by create_date from last search
     * @return activeRow[]
     */
    public function getRepositoriesByLastSearch()
    {
        
        $res=$this->database->table('github_repo')->where("search_id",$this->searchId)->order("create_date DESC");
        //dump($res->getSql());
        return $res->fetchAll();
    }

    private function saveSearch() {
        $this->searchId = $this->database->table('github_search')->insert([
                    'user' => $this->user,
                    'searchdate' => $this->searchDate,
                    "ip" => $this->ip
                ])->getPrimary();
    }

    private function saveRepo($repoArr) {

        $this->database->table('github_repo')->insert([
            'search_id' => $this->searchId,
            "name" => $repoArr['repo'],
            "description" => $repoArr['description'],
            "url" => $repoArr['url'],
            'create_date' => $repoArr['created']
        ]);
    }

    /**
     * method will return array with repositories which is find over github search 
     * when is paginator==null  then will return all repositories , when is paginator defined, than is used to limit and offset
     * @param type $paginator
     * @return array
     */
    public function getRepositoriesFromDB($paginator = null) {
        if ($paginator == null) {
            $result = $this->database->query($this->searchQ);
        } else {
            $result = $this->database->query($this->searchQ . " LIMIT " . $paginator->getLength() . " OFFSET " . $paginator->getOffset());
        }

        $rows = $result->fetchAll();
        return $rows;
    }

    /**
     * return count of repositories over search github
     * @return type
     */
    public function getRepositoriesCount() {
        $result = $this->database->query($this->searchQ);
        return $result->getRowCount();
    }

    /**
     * return count of search on github
     * @return type
     */
    public function getSearchCount() {
        $result = $this->database->table("github_search")->count();
        return $result;
    }

    /**
     * remove searches and repositories old than user defined hour
     * @param int $hours
     * @return int countOfRemovedRows
     */
    public function removeSearchesOldThanHours($hours) {
        $date = new Nette\Utils\DateTime();
        $date->modify("-$hours hours");
        $removeDate = $date->format("Y-m-d H:i:s");
        $result = $this->database->query("delete from github_repo where search_id in ( select id from github_search where searchdate<'" . $removeDate . "')");
        $result = $this->database->query("delete  from github_search where searchdate<'" . $removeDate . "'");
        $removed = $result->getRowCount();
        return $removed;
    }

}
