<?php

namespace Test;

require 'PresenterTest.phpt';

$container = require __DIR__ . '/../bootstrap.php';


class HomepagePresenterTest extends \Tester\TestCase {

        public function __construct(\Nette\DI\Container $container) {
                $this->tester = new \Test\Presenter($container);
        }

        public function setUp() {
                $this->tester->init('Front:Homepage');
        }

        public function testRenderDefault() {
                $this->tester->testAction('default');
        }

}

id(new HomepagePresenterTest($container))->run();